// import {Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Card, Button} from 'react-bootstrap';


// export default function CourseCard(){
//   return(
//       <Row className="mt-3" mb="3">
//           <Col xs={4} md={12}>
//               <Card className="cardHighlight p-1">
//                   <Card.Body>
//                       <Card.Title>
//                           <h4>Sample Course</h4>
//                           Description:
//                       </Card.Title>
//                       <Card.Text>
//                         <p>
//                           This is a sample course offering.
//                         </p>
//                         <p>
//                           <h5>Price:</h5>PhP 40,000
//                         </p>
//                         <p><Button> Enroll</Button></p>
//                       </Card.Text>
//                   </Card.Body>
//               </Card>
//           </Col>
//   </Row>

//   )
// }


// ACTIVITY SOLUTION # 51
export default function CourseCard({courseProp}) {
    console.log(courseProp)
    // console.log(props);
// result: coursesData[0]
    // console.log(typeof props);
// result: object

// object destructuring
    const {name, description, price} = courseProp;
    // console.log(name);

    // Syntax: const[getter, setter] = useState(initialValueGetter);
    const [count, setCount] = useState(0);
    const [seats, less] = useState(30);
    const [isOpen, setIsOpen] = useState(false);

	const enroll = () => {
		if (count < 30){
			setCount(count + 1);
		};
		if (seats > 0){
			less(seats - 1);
		} else if (seats === 0){
            setIsOpen(true)
			alert(`No more seats available`)
        }
	}
    const unenroll = () => {
        if (count > 0){
            setCount(count - 1);
        }
        if(seats < 30){
            less(seats + 1);
            alert(`You have ${seats} seats left`)
        }
    }

    // const enroll = () => {
    //     if(count < 30){
    //         setCount(count + 1);
    //     };
    //     if(count === 30){
    //         alert(`No more seats available`)
    //     }
    // }

    useEffect(() => {
        if(seats > 0){
            setIsOpen(false);
        }
    }, [seats])

    return (
        <Card className= "mb-4">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}.</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                {/* <Card.Text>Seats Available: {30 - counts}</Card.Text> */}
                <Card.Text>Seats Available: {seats}</Card.Text>
                <Button variant="primary" className="me-3" onClick={enroll} disabled={isOpen}>Enroll</Button>
                {/* <Button variant="primary" onClick={enroll} className="me-3">Enroll</Button> */}
                <Button variant="primary" onClick={unenroll}>Unenroll</Button>
            </Card.Body>
        </Card>
    )
}