// ACTIVITY SOLUTION #52

import {useState, useEffect, useContext} from 'react';
import {Form, Button} from "react-bootstrap";
import { Navigate } from 'react-router-dom';
import UserContext from "../UserContext";

export default function Login(props){
  console.log(props);

  const {user, setUser} = useContext(UserContext)
  console.log(user)

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  console.log(email, password);
  // console.log(email);
  // console.log(password);

  // Validation to enable submit button when all fields are populated and both passwords match
   useEffect(() => {
    if((email !== '' && password !== '') && (password)){

      setIsActive(true);

    } else {
      setIsActive(false);

    }

  }, [email, password]);

  function loginUser(e){

    // Prevent page redirection via form submission
    e.preventDefault();

    localStorage.setItem('email', email);

    setUser({
      email: localStorage.getItem('email')
    });

    // Clearing input fields after submission
      // setter works asynchronously
    setEmail('');
    setPassword('');

    // alert(`${email} has been verified! Welcome back!`);
    alert(`Login Complete`);

  }

  return (
    (user.email !== null) ?
    <Navigate to="/courses"/>
    :
    <>
    <h1>Login Here:</h1>
    <Form onSubmit={e => loginUser(e)}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email Address:</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter Email"
            required
            value={email}
            onChange={e => setEmail(e.target.value)}

            />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter Password"
            value={password}

            onChange={e => setPassword(e.target.value)}
            required
            />
      </Form.Group>

      { isActive ?
        <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
        Login
      </Button>

      :

      <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
        Login
      </Button>

      }

    </Form>
    </>
  )
}