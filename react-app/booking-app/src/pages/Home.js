import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';


export default function Home(){
    return(
        <>
            <Banner/>
            <Highlights/>
            {/* <CourseCard/> */}
        </>
    )
}

// ACTIVITY SOLUTION # 53 BY MAM TINE
// import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';

// export default function Home() {
// 	const data = {
//         title: "Booking-App 182",
//         content: "Enroll the courses that we offer",
//         destination: "/courses",
//         label: "Enroll now!"
//     }

//     return (
//         <>
// 	        <Banner data={data}/>
// 	        <Highlights />
// 		</>

//     )
// }
