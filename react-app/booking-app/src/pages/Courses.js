import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){

  // console.log(coursesData);
  // holds the mock database
  // console.log(coursesData[0]);

  const courses = coursesData.map(course => {
    console.log(course.id)
     return(
      <CourseCard key = {course.id} courseProp ={course}/>
     )
  })

  return(
  <>
    <h1> Courses Available: </h1>

    {courses}

    {/* <CourseCard courseProp = {coursesData[0]}/> */}
  </>
  )
}

// let CourseCard = {
//   courseProp: {
//     id: wdc001,
//     name,
//     description,
//     price,
//     onOffer
//   }
// }