// ACTIVITY SOLUTION # 53

import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function errorPage(){
	return (
		<Row>
			<Col className = "p-5">
				<h1>Page Not Found</h1>
                <p>The page are you looking is not exists</p>
                <Button variant="warning">Go back to <Link to="/">homepage</Link></Button>
			</Col>
		</Row>
	)
}

// ACTIVITY SOLUTION # 53 BY MAM TINE
// import Banner from '../components/Banner';

// export default function Error() {

//     const data = {
//         title: "404 - Not found",
//         content: "The page you are looking for cannot be found",
//         destination: "/homepage",
//         label: "Back home"
//     }

//     return (
//         <Banner data={data}/>
//     )
// }
