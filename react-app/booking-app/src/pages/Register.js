import {useState, useEffect} from 'react';
import {Form, Button} from "react-bootstrap";
// ACTIVITY SOLUTION #54
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';


export default function Register(){

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password1, setPassword1] = useState('');
  const [isActive, setIsActive] = useState(false);

  // ACTIVITY SOLUTION #54
  const[user] = useState(UserContext);

  console.log(email, password, password1);
  // console.log(password);
  // console.log(password1);

  useEffect(() => {
    if((email !== '' && password !== '' && password1 !== '') && (password === password1)){

      setIsActive(true);

    } else {
      setIsActive(false);

    }

  }, [email, password, password1]);

  function registerUser(e){
    e.preventDefault();

    setEmail('');
    setPassword('');
    setPassword1('');
  }

  return (
    // ACTIVITY SOLUTION #54
    (user.email !== null) ?
    <Navigate to="/courses"/>

    :

    <>
    <h1>Register Here:</h1>
    <Form onSubmit={e => registerUser(e)}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email Address:</Form.Label>
          <Form.Control
            type="email"
            placeholder="Please input your email here"
            required
            value={email}
            onChange={e => setEmail(e.target.value)}

            />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Please input your password here"
            required
            value={password}
            onChange={e => setPassword(e.target.value)}
            />
      </Form.Group>

      <Form.Group controlId="password2">
        <Form.Label>Verify Password:</Form.Label>
          <Form.Control
            type="password"
            placeholder="Make sure your passwords match"
            required
            value={password1}
            onChange={e => setPassword1(e.target.value)}
            />
      </Form.Group>
      { isActive ?
        <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
        Register
      </Button>

      :

      <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
        Register
      </Button>

      }

    </Form>
    </>
  )
}


