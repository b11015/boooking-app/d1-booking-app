const coursesData = [
  {
    id: "wdc001",
    name: "PHP-Laravel",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmo. Ut Duis aute irure dolor in reprehenderit in voluptate velit esse. Excepteur sint occaecat cupidatat non.",
    price: 45000,
    onOffer: true
  },
  {
    id: "wdc002",
    name: "Python-Django",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmo. Ut Duis aute irure dolor in reprehenderit in voluptate velit esse. Excepteur sint occaecat cupidatat non.",
    price: 50000,
    onOffer: true
  },
  {
    id: "wdc003",
    name: "Java-Springboot",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmo. Ut Duis aute irure dolor in reprehenderit in voluptate velit esse. Excepteur sint occaecat cupidatat non.",
    price: 55000,
    onOffer: true
  }
]

export default coursesData;