// import {Button, Row, Col} from 'react-bootstrap';

// export default function Banner(){
//     return (
//         <Row >
//             <Col className = "p-5" >
//                 <h1>Booking App-182</h1>
//                 <p>Enroll courses here!</p>
//                 <Button variant="primary">Enroll Now!</Button>
//             </Col>
//         </Row>
//     )
// }


// ACTIVITY SOLUTION # 53 BY MAM TINE
import { Link } from 'react-router-dom'
import {Button, Row, Col} from 'react-bootstrap';

export default function Banner({data}){

	console.log(data)
    const {title, content, destination, label} = data;

	return (
		<Row>
			<Col className = "p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="success" as= {Link} to={destination}>{label}</Button>
			</Col>
		</Row>
	)
}