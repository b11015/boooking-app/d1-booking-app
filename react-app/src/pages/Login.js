// // ACTIVITY SOLUTION #52

// import {useState, useEffect, useContext} from 'react';
// import {Form, Button} from "react-bootstrap";
// // import { Navigate } from 'react-router-dom';
// import UserContext from "../UserContext";

// export default function Login(props){
//   console.log(props);

//   const {user, setUser} = useContext(UserContext)
//   console.log(user)

//   // State hooks to store the values of the input fields
//   const [email, setEmail] = useState('');
//   const [password, setPassword] = useState('');
//   // State to determine whether submit button is enabled or not
//   const [isActive, setIsActive] = useState(true);

//   console.log(email, password);
//   // console.log(email);
//   // console.log(password);

//   // Validation to enable submit button when all fields are populated and both passwords match
//    useEffect(() => {
//     if((email !== '' && password !== '') && (password)){

//       setIsActive(true);

//     } else {
//       setIsActive(false);

//     }

//   }, [email, password]);

//   function loginUser(e){

//     // Prevent page redirection via form submission
//     e.preventDefault();

//     fetch('http://localhost:4000/users/login', {
//       method: "POST",
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         email: email,
//         passwrord: password
//       })
//     })
//     .then(res => res.json())
//     .then(data => {
//       console.log(data);

//       if(typeof data.accessToken !== "undefined"){
//          localStorage.setItem('token', data.accessToken);
//        }
//     })

//     localStorage.setItem('email', email);

//     setUser({
//       email: localStorage.getItem('email')
//     });

//     // Clearing input fields after submission
//       // setter works asynchronously
//     setEmail('');
//     setPassword('');

//     // alert(`${email} has been verified! Welcome back!`);
//     alert(`Login Complete`);

//   }

//   return (
//     // (user.email !== null) ?
//     // <Navigate to="/courses"/>
//     // :
//     <>
//     <h1>Login Here:</h1>
//     <Form onSubmit={e => loginUser(e)}>
//       <Form.Group controlId="userEmail">
//         <Form.Label>Email Address:</Form.Label>
//           <Form.Control
//             type="email"
//             placeholder="Enter Email"
//             required
//             value={email}
//             onChange={e => setEmail(e.target.value)}

//             />
//           <Form.Text className="text-muted">
//             We'll never share your email with anyone else.
//           </Form.Text>
//       </Form.Group>

//       <Form.Group controlId="password">
//         <Form.Label>Password:</Form.Label>
//           <Form.Control
//             type="password"
//             placeholder="Enter Password"
//             value={password}

//             onChange={e => setPassword(e.target.value)}
//             required
//             />
//       </Form.Group>

//       { isActive ?
//         <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
//         Login
//       </Button>

//       :

//       <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
//         Login
//       </Button>

//       }

//     </Form>
//     </>
//   )
// }

// 123123123

// ACTIVITY SOLUTION #52

import { useState, useEffect, useContext } from 'react';
import { Form, Button, } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import {Link}  from 'react-router-dom';
import UserContext from '../UserContext'
import  Swal from 'sweetalert2';

export default function Login(props) {
    console.log(props);

    const {user, setUser} = useContext(UserContext)
    console.log(user)

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    console.log(email, password);
    // console.log(email);
    // console.log(password);


    function loginUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.accessToken !== "undefined"){
              localStorage.setItem('token', data.accessToken)
              retrieveUserDetails(data.accessToken)

              Swal.fire({
                title: 'Login Successful',
                icon: 'success',
                text: 'Welcome to Booking App of 182!'
              })
            } else {
              Swal.fire({
                title: 'Login Failed',
                icon: 'error',
                text: 'Please check your email and password'
              })
            }
        })

        // localStorage.setItem('email', email)

        // setUser({
        //     email: localStorage.getItem('email')
        // });

        // Clear input fields after submission
        // setter works asynchronously
        setEmail('');
        setPassword('');

        // alert(`${email} has been verified! Welcome back!`);

    }

    const retrieveUserDetails = (token) => {
      fetch('http://localhost:4000/users/getUserDetails', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data)

        setUser({
          _id: data._id,
          isAdmin: data.isAdmin,
        })
      })
    }

useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

}, [email, password]);

    return (

        (user.id !== null) ?
        <Navigate to= "/courses"/>
        :

        <>
        <h1>Login Here:</h1>
        <Form onSubmit={e => loginUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            <p>Not yet registered<Link to="/register"> Register Here</Link></p>

             { isActive ?
                <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
                    Submit
                </Button>
            }
        </Form>
        </>

    )

}